all : 
	echo "Usage: make [tag|push]"

tag: 
	find . -type f -name '*.php' > cscope.files
	cscope -b
	ctags -R --languages=php --regex-PHP='/(public |static |abstract |protected |private )+function ([^ (]*)/\2/f/' 

push :
	git push ssh://repo.or.cz/srv/git/mahara-contrib.git adminlang-1.0:adminlang-1.0
	git push git+ssh://git.mahara.org/git/contrib/adminlang adminlang-1.0:adminlang-1.0
