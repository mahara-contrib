{include file='header.tpl' nosearch='true'}

{include file="columnfullstart.tpl"}

    <div id="adminlangreport">

        <div id="reportselectionwrapper">
            <form id="reportselectionform" method="get">
                {str tag=thislanguage} /
                <select name="report" onchange="$('reportselectionform').submit()">
                    {html_options values=$report_ids output=$report_names selected=$reportid}
                </select>
                <input type="submit" id="choosereportbutton" value="{str section=admin tag=langchoosereport}" />
                <script type="text/javascript">hide_element('choosereportbutton');</script>
            </form>
        </div>
        
        {if $reportid eq "missing"}
            {include file="admin/langreport-missing.tpl}
        {/if}
        

    </div>

{include file="columnfullend.tpl"}

{include file='admin/upgradefooter.tpl'}
