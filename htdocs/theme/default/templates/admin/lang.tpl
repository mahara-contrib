{include file='header.tpl' nosearch='true'}

{include file="columnfullstart.tpl"}
	
        <div id="adminlang">
            <form id="fileselectionform" method="get">
                {str tag=thislanguage} / 
                <select name="file" onchange="$('fileselectionform').submit()">
                    {html_options values=$lang_files output=$lang_filenames selected=$current_file}
                </select>
                <input type="submit" id="loadfilebutton" value="{str section=admin tag=strloadfile}" />
                <script type="text/javascript">hide_element('loadfilebutton');</script>
            </form>
            {if $lang_strings}
            <form id="allstrings" method="POST" action="{$save_all_handler}">
            <input type="hidden" id="current_file" name="file" value="{$current_file}" />
			<table cellspacing="0" cellpadding="1" id="strings" wrap="yes">
                <thead>
                    <tr>
                        <th>{str section=admin tag=stringoriginal}</th>
                        <th>{str section=admin tag=stringtranslated}</th>
                        <th id="msgscol">{str section=admin tag=stringstatus}</th>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$lang_strings key=stringid item=string}
                    <tr class="{cycle name=rows values=r1,r0}">
                        <td class="original">
                            <div class="text">{$string->original}</div>
                            <div class="identifier">{$string->id}</div>
                        </td>
                        {if $string->translated == ''} 
                        <td id="translated_{$string->elementid}" class="translated empty">
                        {else}
                        <td id="translated_{$string->elementid}" class="translated">
                        {/if}
                        <div class="static hidden" id="static_{$string->elementid}" onClick="javascript:toggle_edit('{$string->elementid}',true);">
                            {$string->translated}
                        </div>
                        <textarea name="strings[{$string->elementid}]" rows="3" cols="40" id="text_{$string->elementid}" onChange="queue_string('{$string->elementid}')" onFocus="string_focus('{$string->elementid}')" >{$string->translated}</textarea>
                        {if $string->translated == ''}
                            <script type="text/javascript">toggle_edit('{$string->elementid}',true);</script>
                        {else}
                            <script type="text/javascript">toggle_edit('{$string->elementid}',false);</script>
                        {/if}
                        </td>
                        <td id="msg_{$string->elementid}" class="msgscol">&nbsp;</td>
                    </tr>
                {/foreach}
                </tbody>
			</table>
            <input type="submit" value="{str section=admin tag=savechanges}" id="savechangesbutton" />
            <script type="text/javascript">hide_element('savechangesbutton');</script>
            </form>
            {/if}
        </div>

{include file="columnfullend.tpl"}

{include file='admin/upgradefooter.tpl'}
