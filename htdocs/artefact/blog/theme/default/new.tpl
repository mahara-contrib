{*

  This template displays the 'new blog' form

 *}

{include file="header.tpl"}

{include file="sidebar.tpl"}

{include file="columnleftstart.tpl"}
    		<h2>{str section="artefact.blog" tag="newblog"}</h2>
    		{$newblogform}

{include file="columnleftend.tpl"}
{include file="footer.tpl"}
