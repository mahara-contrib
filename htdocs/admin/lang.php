<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage admin/lang
 * @author     David Mudrak <david.mudrak@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'adminlang/translate');

require(dirname(dirname(__FILE__)).'/init.php');
require(get_config('libroot') . 'lang.php');

$current_file = param_variable('file', '');
$strings = param_variable('strings', null); // strings to save at once

if ($strings) {
    // save all strings at once
    try {
        language_save_string(current_language(), $current_file, '', '', $strings);
        $SESSION->add_info_msg('Strings saved');    // todo translate
    }
    catch (LanguageException $e) {
        $SESSION->add_error_msg('Error saving file'); // todo translate
    }
    redirect(get_config('wwwroot') . 'admin/lang.php?file='.urlencode($current_file));
}

$DOCROOT = get_config('docroot');
while (substr($DOCROOT, -1) == '/') {
    $DOCROOT = substr_replace($DOCROOT, '', -1);
}

define('TITLE', get_string('administration', 'admin'));

$savingicon = theme_get_url('images/loading.gif');
$successicon = theme_get_url('images/success.gif');
$failureicon = theme_get_url('images/failure.gif');

$savingstring = json_encode(get_string('stringsaving', 'admin'));
$successstring = json_encode(get_string('stringsaved', 'admin'));
$failurestring = json_encode(get_string('stringunsaved', 'admin'));

$smarty = smarty();

$js = <<< EOJS
    var strings_in_queue = new Array();
    var running = false;
    
    /**
     * Adds a string to the process queue
     *
     * @uses strings_in_queue
     */
    function queue_string(id) {
        var msgbox = $('msg_' + id);

        msgbox.innerHTML = '<img src="{$savingicon}" alt="' + {$savingstring} + '" />';
        strings_in_queue.push(id);
        if (!running) {
            process_next_in_queue();
        }
    }

    /**
     * If there is no other string being saved, try and save another one from the queue
     *
     * @uses strings_in_queue
     * @uses running
     */
    function process_next_in_queue() {
        if (!running) {
            var nextstring = strings_in_queue.shift();
            if (nextstring) {
                save_string(nextstring);
            }
        }
    }

    /**
     * When the user starts to type something into the field
     */
    function string_focus(id) {
        var msgbox = $('msg_' + id);

        if (!running) {
            msgbox.innerHTML = '';
        }
    }

    /**
     * Replace a token in a string
     *
     * @param string t Token to be found and removed
     * @param string u Token to be inserted
     * @param string s String to be processed
     * @returns string New string
     */
    function str_replace(t, u, s) {
        i = s.indexOf(t);
        r = "";
        if (i == -1) {
            return s;
        }
        r += s.substring(0,i) + u;
        if ( i + t.length < s.length) {
            r += str_replace(s.substring(i + t.length, s.length), t, u);
        }
        return r;
    }

    /**
     * Add class "hidden" to the given element
     * 
     * @param string id Element ID
     */
    function hide_element(id) {
        var e = $(id);  // element

        e.className = e.className + ' hidden';
    }

    /**
     * Remove class "hidden" from the given element
     *
     * @param string id Element ID
     */
    function show_element(id) {
        var e = $(id);  // element

        e.className = str_replace('hidden', '', e.className);
    }

    function toggle_edit(id, edit) {

        if (edit) {
            show_element('text_' + id);     // show <textarea>
            hide_element('static_' + id);   // hide static text
        } else {
            hide_element('text_' + id);     // ... and vice-versa
            show_element('static_' + id);
        }
    }

    /**
     * Save a string via JSON request
     *
     * @param str id The string identifier
     * @uses running
     */
    function save_string(id) {
        var msgbox = $('msg_' + id);            // status table cell
        var transbox = $('translated_' + id);   // table cell with textarea   
        var textarea = $('text_' + id);           // textarea
        var text = textarea.value;       // the value of textarea
        var stext = $('static_' + id);          // static text containg the saved translation
        var current_file = $('current_file').value;

        if (!current_file) {
            msgbox.innerHTML = '<img src="{$failureicon}" alt=":(" /> ';
            return;
        }
        running = true;
        sendjsonrequest(
            'lang.json.php',
            {'action': 'save', 'file': current_file, 'stringid': id, 'text': text }, 
            'POST', 
            function (data) {
                var message;
                if ( !data.error ) {
                    // message = {$successstring};
                    msgbox.innerHTML = '<img src="{$successicon}" alt=":)" />  ';
                    if (data.savedtext == "") {
                        transbox.className = transbox.className + ' empty';
                    } else {
                        transbox.className = str_replace('empty', '', transbox.className);
                        stext.innerHTML = data.savedtext;
                        toggle_edit(id, false);
                    }
                }
                else {
                    if (data.errormessage) {
                        message = data.errormessage;
                    }
                    else {
                        message = {$failurestring};
                    }
                    msgbox.innerHTML = '<img src="{$failureicon}" alt=":(" /> ' + message;
                }
                running = false;
                process_next_in_queue();
            }, 
            '',
            true);
    }
EOJS;
$smarty->assign('INLINEJAVASCRIPT', $js);

// Get list of lang packs - try cached first
$lang_dirs = $SESSION->get('admin_lang_directories');
if (empty($lang_dirs)) {
    // no cache found - rebuild the list
    try {
        $lang_locations = array('lang');                                 // core language files
        $lang_locations = array_merge($lang_locations, plugin_types());  // plugin language files
        $lang_dirs = get_lang_directories($lang_locations);
        $SESSION->set('admin_lang_directories', $lang_dirs);
    }
    catch (Exception $e) {
        log_info($e->getMessage());
        log_info($e->getTrace());
        $SESSION->add_error_msg("An error occurred while getting locations of lang packs: " . $e->getMessage());
    }
}

$lang_files = array(''=> get_string('strchoosefile', 'admin'));
foreach ($lang_dirs as $lang_dir) {
    $files = get_directory_list(implode('/', array($DOCROOT, $lang_dir, 'lang', 'en.utf8')));
    foreach($files as $file) {
        if ((substr($file, -4) == ".php")) {
            if (empty($lang_dir)) {
                $lang_files[implode('/', array('lang', 'en.utf8', $file))] = $file;
            } else {
                $lang_files[implode('/', array($lang_dir, 'lang', 'en.utf8', $file))] = $file;
            }
        }
    }
}
$smarty->assign('lang_files', array_keys($lang_files));
$smarty->assign('lang_filenames', array_values($lang_files));
$smarty->assign('current_language', current_language());
$smarty->assign('current_file', $current_file);
$smarty->assign('save_all_handler', get_config('wwwroot') . 'admin/lang.php?file='.urlencode($current_file));

$lang_strings = array();
if (!empty($current_file)) {
    unset($string);
    include(implode('/', array($DOCROOT, $current_file)));
    $enstrings = language_load_strings('en.utf8', $current_file);
    if (is_array($enstrings)) {
        $trstrings = language_load_strings(current_language(), $current_file);
        $SESSION->add_info_msg('Loaded '.$current_file . ' ('.count($enstrings).' strings)'); // todo translate
        foreach ($enstrings as $stringid => $original) {
            $lang_strings[$stringid] = new stdClass();
            $lang_strings[$stringid]->id = $stringid;
            $lang_strings[$stringid]->elementid = str_replace('/', '::', $stringid);
            $lang_strings[$stringid]->original = $original;
            if (! empty($trstrings[$stringid])) {
                $lang_strings[$stringid]->translated = $trstrings[$stringid];
            } else {
                $lang_strings[$stringid]->translated = '';
            }
            $lang_strings[$stringid]->status = '';
        }
    }
} else {
    $smarty->assign('current_file', '');
}
$smarty->assign_by_ref('lang_strings', $lang_strings);

$smarty->display('admin/lang.tpl');

?>
